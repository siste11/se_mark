---
title: "El Estudio del Proceso de Busqueda de Productos en Linea Usando Metodologia DELPHY"
date: "Jan 2018 (last updated: Jan 2018)"
author: "Juan Felipe Diaz, Leidy Johana Bravo, Oscar Ernesto Munoz"
---

----

# Resumen

La aparición de los mercados en linea, ha cambiado la forma de interactuar con los proveedores de diferentes productos. Cada vez mas, se impone el me todo de compra en linea como me todo de abastecer las necesidades particulares. Sin embargo, y aunque la compra en linea hace parte de un sistema informático, siempre existen alternativas para adquirir diferente productos, queremos analizar las decisiones que se toman en torno a donde, cuando y como se compran estos productos. Trataremos de determinar los parámetros que expertos usan para realizar este tipo procesos, también como las formas para calificar estas actividades y en general adquirir y plasmar en un sistema experto todo esta información, para lograr este objetivo usaremos y analizaremos métodos como Delphy entre otros.

->

# Abstract

The emergence of online markets, has changed the way of interacting with suppliers of different products. More and more, the online shopping method is imposed as everything to supply the particular needs. However, although online shopping is part of a computer system, there are always alternatives to purchase different products, we want to analyze the decisions that are made about where, when and how these products are purchased. We will try to determine the parameters that experts use to perform this type of process, as well as the ways to qualify these activities and in general acquire and translate into an expert system all this information, to achieve this goal we will use and analyze methods such as Delphy among others.

----

## Palabras clave:
Auditoría, encuesta, sistema, mercado, experto, Delphi.

----

# 1. Introducción 

En este documento se mostrará la guía de qué manera debemos llevar a cabo una gestión de encuestas para la realización ya sea de una auditoría o algún fin común, en este tendremos a disposición el método delphi, herramienta especializada en la toma de decisiones por medio de encuestas guiadas a expertos, el cual tiene 4 fases y 3 ciclos de preguntas, acá reunimos información para tomar una acertada solución a un problema específico. 

->

La auditoría puede ser considerada como un control técnico que proporciona a la dirección un método para evaluar la eficacia en procedimientos operativos y controles internos (Vilar Barrio - 1999). La auditoría es necesaria además para realizar mediciones que puedan solucionar algún problema o ayuden a tomar una decisión es por esto que la medición del desempeño mediante indicadores solo ayuda a la identificación de problemas existentes, analizando el histórico de una operación, pero se considera necesaria la identificación de las causas de estos problemas para tomar acciones asociadas a su solución(ACEVEDO, J. A.; GÓMEZ - 2007)

->

Para este tipo de mediciones también se puede llevar a cabo modelos como Delphi que Como en toda técnica de recogida de información, los objetivos del estudio deben estar claramente formulados. Son estos los que darán sentido propositivo, los que orientan las acciones, en ellos están contenidas las variables del estudio y las dimensiones que deben explorarse para su logro.(Reguant-Álvarez,Torrado-Fonseca - 2016)

->

Existen diferentes formas de enfocar una auditoría, sin embargo las más reconocidas y las que se pueden llevar a cabo son las siguientes:

-Auditoría de operaciones
-Auditoría de gestión
-Auditoría de desempeño
-Auditoría de sistemas
-Auditoría de eficiencia
-Auditoría de objetivos

->

Cada una de estas nos ofrece una mirada diferente para la decisión o solución a la que queramos llegar, pero para este tipo de casos se necesita utilizar cuestionarios detallados usados como base para entrevistas con personal de la empresa junto con sus conocimientos frente a Auditoría y las reglas de negocio a auditar,el auditor puede encontrar áreas de ineficiencia dentro de la empresa.

->

Las compras en linea son parte integral de la vida cotidiana actualmente, la forma en que los sistemas dedicados a los mercados en linea crecen exponencialmente, nuevas técnicas y métodos son usados casi a diario para entender a los clientes y sus actividades comerciales en linea. Muchas de las herramientas de comercio en linea han puesto el foco en sistemas que permitan a sus clientes obtener sus productos realizando búsquedas eficientes que equilibren calidad y precio, esto hace de sus vitrinas mas visitadas y por ende incrementa sus utilidades.

->

Implementar herramientas y técnicas son necesarias para incrementar la eficiencia de los mercados en linea, mas cuando los recursos son limitados. Sin embargo, existe un curva de aprendizaje para nuevos clientes, en varios sentidos; la búsqueda del equilibrio entre calidad/precio hace que se pierda en algunas ocasiones recursos monetarios o inclusive se puede llegar a obtener artículos inservibles.

->

También existen gran variedad de sitios que presentan ventajas y desventajas, la presencia de alternativas locales tiene un efecto positivo cuando los clientes buscan representación y soporte, también el tiempo de acceso a los artículos hace que estos “puestos” locales sean una opción al momento de la búsqueda.

----

# 2. Metodología Delphi

El primer estudio de Delphi fue realizado en 1950 por la Rand Corporation para la fuerza aérea de Estados Unidos, y se le dio el nombre de Proyecto Delphi. Su objetivo era la aplicación de la opinión de expertos a la selección de un sistema industrial norteamericano óptimo y la estimación del número de bombas requeridas para reducir la producción de municiones hasta un cierto monto.

->

Las características principales del método Delphi son las siguientes:
 
– Anonimato: Durante el Delphi ningún experto conoce la identidad de los otros que componen el grupo de debate.

->

– Iteración y realimentación controlada: La iteración se consigue al presentar varias veces el mismo cuestionario, lo que permite disminuir el espacio intercuartil, ya que se consigue que los expertos vayan conociendo los diferentes puntos y puedan ir modificando su opinión.

->

– Respuesta del grupo en forma estadística: La información que se presenta a los expertos no es solo el punto de vista de la mayoría sino que se presentan todas las opiniones indicando el grado de acuerdo que se ha obtenido.

->
 
– Heterogeneidad: Pueden participar expertos de determinadas ramas de actividad sobre las mismas bases.

----
 
El método Delphi consta de 4 fases para su ejecución, estas se componen de la siguiente manera:
 
- Definición de objetivos: En esta primera fase se plantea la formulación del problema y un objetivo general que estaría compuesto por el objetivo del estudio, el marco espacial de referencia y el horizonte temporal para el estudio.

->

- Selección de expertos: Esta fase presenta dos dimensiones:

-> 

Dimensión cualitativa: Se seleccionan en función del objetivo prefijado y atendiendo a criterios de experiencia posición responsabilidad acceso a la información y disponibilidad.

->

Dimensión Cuantitativa: Elección del tamaño de la muestra en función de los recursos medios y tiempo disponible. 
Formación del panel,Se inicia la fase de captación que conducirá a la configuración de un panel estable. En el contacto con los expertos conviene informarles de:
– Objetivos del estudio
– Criterios de selección
– Calendario y tiempo máximo de duración
– Resultados esperados y usos potenciales
– Recompensa prevista (monetaria, informe final, otros)

- Elaboración y lanzamiento de los cuestionarios: Los cuestionarios se elaboran de manera que faciliten la respuesta por parte de los encuestados. Las respuestas habrán de ser cuantificadas y ponderadas (año de realización de un evento, probabilidad de un acontecimiento…)

->

- Explotación de resultados: El objetivo de los cuestionarios sucesivos es disminuir la dispersión y precisar la opinión media consensuada. En el segundo envío del cuestionario, los expertos son informados de los resultados de la primera consulta, debiendo dar una nueva respuesta. Se extraen las razones de las diferencias y se realiza una evaluación de ellas. Si fuera necesario se realizaría una tercera oleada.

----

# 3. Mercados en linea

Los mercados en linea son plataformas que actúan como un intermediario para poner en contacto compradores y vendedores. Estas plataformas varían en su tipo de intervención, por ejemplo algunas son gratuitas, otras tienen comisión sobre el producto y otras pueden cobrar por algún tipo de membresía.  

->

También se presentan otro tipos de tipologías para los mercados electrónicos, dependiendo de los siguientes aspectos:

- Orientación (vendedores, compradores).
- Abiertos a la participación de cualquier empresa.
- Dirigidos a una industria concreta (vertical).

->

## 3.1 Sistemas de información comprometidos con el desarrollo

Para desarrollar un sistema experto, se necesita de un sistema de información, la construcción de estos sistemas de información pueden ser en si mismos un proyecto completo de implementación, esto esta fuera del alcance de este proyecto, este proyecto busca realizar el análisis de diferentes tipos de artículos y comparar desde los vendedores hasta la forma de entrega, toda esa gama de propiedades que puede presentar un articula estará dictada por la información que aporten los expertos. 

->

Sin un sistema de información que nos provea de datos de artículos reales y de productos que existan realmente, sin esa información este proyecto seria practicante imposible, e inclusive de cierta forma inútil. Sin embargo, es posible acceder a estas bases de datos de artículos reales haciendo uso de APIs tipo RestFul, sin embargo es necesario validar en profundidad si la información proporcionada es útil y/o completa.

> TODO: profundizar en RestFul

-> 

Una de las principales tiendas online en latino América es es Mercado Libre, con mas de 145 millones de usuarios registrados, siendo el primer sitio mas visitado en Latino América y el 8 a nivel mundial. Mercado libre, cuenta con una APIs RestFul, donde su principales beneficios son:

- Acceder mediante cualquier lenguaje de programación.
- Integridad en los datos de artículos y entidades.
- Los datos están formateados en formato JSON.
- Se puede ingresar por medio de diferentes recursos (users, items, orders, etc.)

> TODO: profundizar en JSON

->

El uso de esta interfase durante este proyecto estará sustentada por la necesidad de 
- Acceder a información de artículos de forma masiva.
- Sincronizar automáticamente.
- Ver comentarios y datos relacionados con los artículos.
- Analizar precios.
- Acceder a la información de contacto y ubicación de los vendedores.

->

### 3.2 RESTful API

Una API RESTful es una interfase de aplicación (API), que usa los TAGs del protocolo HTTP (GET, PUT, POST y DELETE) para dar acceso general a los datos, se usa cuando no se puede saber de antemano que tipo de lenguaje u aplicación puede llegar a tener acceso.

->

### 3.3 Formato JSON

Es un formato de texto ligero para intercambio de datos principalmente, suele usarse para incluir Notación de Objetos, su adopción principalmente se dio como alternativa al formato XML pues ha sido ampliamente utilizado en desarrollos AJAX e integrado dentro del interprete de JavaScript de cualquier navegador.

-> 

----

### 4 Face-to-face meetings (FTF)

Reuniones cara a cara es un acercamiento comun para un grupo de toma de desiciones, soporta cualquier forma de interaccion entre sus miembros.

->

El objetivo es generar una no estructurada discucion y estimar la solucion para un problema particular.

->

Este sistema adolece de los siguiente problemas:

- Requiere un gran ezfurzo para mantener el grupo de expertos
- Estos grupos tienden a tomar "desiciones rapidas" y no considerar todas la dimenciones
- Grupos tienden a tener un efecto de tendencia central
- La gente mas timida tiende a aceptar otras opiniones
- Las personas dominates tienden a a influenciar demaciado
- Se establecen relaciones de competenciai

->

En algunas situaciones, la interaccion personal en grupos suele ser coherente, adicionalmente, en general la gente disfruta la interaccion humana, lo que hace el trabajo satisfactorio.

----

# Concluciones

- Siguiendo las normas del método Delphi podemos llegar a ver más claramente un camino o solución a un problema dado, además de dar grandes beneficios para toma de decisiones futuras.

# References

