#!/usr/bin/env sh

pandoc --filter pandoc-citeproc --bibliography=references.bib --variable classoption=twocolumn --variable papersize=a4paper -s Readme.md -o Readme.pdf

